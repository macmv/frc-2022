package frc.team3681.robot;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Comparator;

public class Vision {
    public static class VisionResult {
        int x;
        int y;

        int offset() {
            return 70 - x;
        }
        int distance() {
            return 110 - y;
        }
    }

    static ArrayList<Mat> channels = new ArrayList<>();
    static Mat blur_r = new Mat();
    static Mat blur_g = new Mat();
    static Mat blur_b = new Mat();
    static Size BLUR_SIZE = new Size(2, 2);
    static VisionResult res = new VisionResult();
    static ArrayList<MatOfPoint> contours = new ArrayList<>();

    static VisionResult draw_contours(Mat frame) {
        var max = contours.stream().max(Comparator.comparingDouble(a -> a.size().area()));
        if (max.isEmpty()) {
            return res;
        }
        var c = max.get();
        var rect = Imgproc.boundingRect(c);
        Imgproc.rectangle(frame, rect, Scalar.all(0x00ff00));
        // Imgproc.drawContours(frame, List.of(c), -1, Scalar.all(255), 1);

        res.x = rect.x + rect.width / 2;
        res.y = rect.y + rect.height / 2;
        return res;
    }

    static VisionResult process_image(Mat frame) {
        channels.clear();
        contours.clear();
        Core.split(frame, channels);
        var r = channels.get(0);
        var g = channels.get(1);
        var b = channels.get(2);

        Imgproc.blur(r, blur_r, BLUR_SIZE);
        Imgproc.blur(g, blur_g, BLUR_SIZE);
        Imgproc.blur(b, blur_b, BLUR_SIZE);

        for (Mat m : channels) {
            m.release();
        }

        // Turns r into a mask
        Imgproc.threshold(blur_r, blur_r, 120, 255, Imgproc.THRESH_BINARY);
        // Turns b into a mask
        Imgproc.threshold(blur_b, blur_b, 120, 255, Imgproc.THRESH_BINARY);

        Core.bitwise_xor(frame, frame, frame, blur_r);
        Core.bitwise_xor(frame, frame, frame, blur_b);
        // `blur_r` is our mask. We xor that with itself using an inverted mask of `blur_r` and `blur_b`
        // This will set all the white pixels in `blur_r` and `blur_b` to black in `blur_g`.
        Core.bitwise_xor(blur_g, blur_g, blur_g, blur_r);
        Core.bitwise_xor(blur_g, blur_g, blur_g, blur_b);

        // We've now removed the white pixels from blur_g, so we create a mask from it and find contours.
        Imgproc.threshold(blur_g, blur_g, 128, 255, Imgproc.THRESH_BINARY);
        Imgproc.findContours(blur_g, contours, blur_g, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        return draw_contours(frame);
    }
}
