package frc.team3681.robot;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.ControlType;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.hal.HAL;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import org.opencv.core.Mat;

public class Robot extends TimedRobot {
    boolean areRedAlliance = true;

    MotorController frontLeft = new SparkWrapper(1);
    MotorController backLeft = new SparkWrapper(8);
    MotorController frontRight = new SparkWrapper(2);
    MotorController backRight = new SparkWrapper(9);
    VictorSPX ballIntake = new VictorSPX(4);
    VictorSPX ballLift = new VictorSPX(5);
    CANSparkMax shoot = new CANSparkMax(3, MotorType.kBrushless);
    VictorSPX liftLeft = new VictorSPX(10);
    VictorSPX liftRight = new VictorSPX(11);

    MecanumDrive drive = new MecanumDrive(frontLeft, backLeft, frontRight, backRight);
    Joystick xbox = new Joystick(0);
    Joystick otherStick = new Joystick(1);

    DigitalOutput lightControl = new DigitalOutput(2);
    DigitalOutput redblueLightControl = new DigitalOutput(1);
    public static class SparkWrapper implements MotorController {
        private final CANSparkMax inner;
        private boolean inverted;
        SparkWrapper(int device) {
            inner = new CANSparkMax(device, MotorType.kBrushless);

            inner.getPIDController().setP(0.0001);
            inner.getPIDController().setI(1e-20);
            inner.getPIDController().setD(0.0005);
            inner.getPIDController().setFF(0.00017);
        }

        @Override
        public void set(double speed) {
            if (inverted) {
                speed *= -1;
            }
            inner.getPIDController().setReference(speed * 5000, CANSparkMax.ControlType.kVelocity);
        }

        @Override
        public double get() {
            return inner.get();
        }

        @Override
        public void setInverted(boolean isInverted) {
            inverted = isInverted;
        }

        @Override
        public boolean getInverted() {
            return inverted;
        }

        @Override
        public void disable() {}

        @Override
        public void stopMotor() {}
    }

    private final Lock dataLock = new ReentrantLock();
    // offset and distance must both be accessed under a `data_lock` lock!
    private float offset;
    private float distance;

    @Override
    public void robotInit() {
        shoot.getPIDController().setP(0.0001);
        shoot.getPIDController().setI(1e-20);
        shoot.getPIDController().setD(0.0005);
        shoot.getPIDController().setFF(0.00017);
        var visionThread = new Thread(() -> {
            var cam = CameraServer.startAutomaticCapture(1);
            var drive = CameraServer.startAutomaticCapture(0);
            drive.setBrightness(100);
            drive.setExposureManual(40);
            drive.setWhiteBalanceManual(5000);
            cam.setBrightness(100);
            cam.setExposureManual(40);
            cam.setWhiteBalanceManual(5000);
            if (true) { return; }
            cam.setBrightness(0);
            cam.setExposureManual(20);
            cam.setWhiteBalanceManual(20000);
            var sink = CameraServer.getVideo(cam);
            var output = CameraServer.putVideo("processed", 320, 240);

            var frame = new Mat();
            while (!Thread.interrupted()) {
                if (sink.grabFrame(frame) == 0) {
                    output.notifyError(sink.getError());
                    continue;
                }
                var res = Vision.process_image(frame);
                dataLock.lock();
                this.offset = res.offset();
                this.distance = res.distance();
                dataLock.unlock();
                output.putFrame(frame);
            }

            Vision.process_image(frame);
        });
        visionThread.setDaemon(true);
        visionThread.start();

        backLeft.setInverted(true);
        frontLeft.setInverted(true);

        lightControl.set(false);
        setTeamLight();
    }

    Timer timer = new Timer();

    @Override
    public void autonomousInit() {
        setTeamLight();
        timer.reset();
        timer.start();
    }

    @Override
    public void teleopInit() {
        setTeamLight();
    }

    private void setTeamLight() {
        redblueLightControl.set(HAL.getAllianceStation().name().contains("Red"));
    }

    @Override
    public void disabledPeriodic() {

    }

    @Override
    public void autonomousPeriodic() {
        dataLock.lock();
        var offset = this.offset;
        var distance = this.distance;
        dataLock.unlock();

        System.out.println("offset: " + offset);
        System.out.println("distance: " + distance);

        var time = timer.get();
        if (time < 1) {
            // 0 - 1
            fire(true);
        } else if (time < 3) {
            // 1 - 3
            fire(true);
            ballLift.set(VictorSPXControlMode.PercentOutput, -1);
        } else if (time < 5.5) {
            // 3 - 5.5
            fire(false);
            ballLift.set(VictorSPXControlMode.PercentOutput, -1);
            ballIntake.set(VictorSPXControlMode.PercentOutput, 0.5);
            drive(0.6, 0, 0.12);
            // drive(0, 0, 0);
        } else if (time < 8.1) {
            // 5.5 - 8.1
            ballLift.set(VictorSPXControlMode.PercentOutput, 0);
            ballIntake.set(VictorSPXControlMode.PercentOutput, 0);
            drive(-0.6, 0.08, -0.12);
            // drive(0, 0, 0);
        } else if (time < 8.5) {
            // 8.1 - 8.5
            fire(true);
            drive(0, 0, 0);
        } else if (time < 11) {
            // 8.5 - 11
            fire(true);
            ballLift.set(VictorSPXControlMode.PercentOutput, -1);
            drive(0, 0, 0);
        } else {
            // 11 -
            fire(false);
            ballLift.set(VictorSPXControlMode.PercentOutput, 0);
            drive(0.5, 0, 0); // get out of starting area
        }
        System.out.println("time: " + time);
    }

    @Override
    public void teleopPeriodic() {
        dataLock.lock();
        var offset = this.offset;
        var distance = this.distance;
        dataLock.unlock();

        // System.out.println("offset: " + offset);
        // System.out.println("distance: " + distance);

        // A
        if (xbox.getRawButton(0)) {
            driveAuto(offset, distance);
        } else {

            //forward and reverse
            var y = applyDeadzone(xbox.getRawAxis(1));

            //turn
            var z = applyDeadzone(xbox.getRawAxis(0));
            var leftTrigger = xbox.getRawAxis(2);
            var rightTrigger = xbox.getRawAxis(3);

            //strafe
            var x = applyDeadzone(xbox.getRawAxis(4));

            if (!xbox.getRawButton(6)) {
                y *= -1;
                x *= -1;
            }

            drive(y, x, z);
        }
        /*
        liftLeft.set(VictorSPXControlMode.PercentOutput, leftStick.getY() * 1.0);
        liftRight.set(VictorSPXControlMode.PercentOutput, -rightStick.getY() * 1.0);
        */


        if (otherStick.getRawButton(5) && otherStick.getRawButton(9) && otherStick.getY() < 0) {
            // RUNS IT IN REVERSE
            liftLeft.set(VictorSPXControlMode.PercentOutput, otherStick.getY() * -0.5);
        } else if (otherStick.getRawButton(7) && otherStick.getY() < 0) {
            // Runs it forwards
            liftLeft.set(VictorSPXControlMode.PercentOutput, otherStick.getY());
        } else {
            liftLeft.set(VictorSPXControlMode.PercentOutput, 0);
        }
        if (otherStick.getRawButton(5) && otherStick.getRawButton(10) && otherStick.getY() < 0) {
            // RUNS IT IN REVERSE
            liftRight.set(VictorSPXControlMode.PercentOutput, otherStick.getY() * -0.5);
        } else if (otherStick.getRawButton(8) && otherStick.getY() < 0) {
            // Runs it forwards
            liftRight.set(VictorSPXControlMode.PercentOutput, otherStick.getY());
        } else {
            liftRight.set(VictorSPXControlMode.PercentOutput, 0);
        }

        // Trigger
        // fire(xbox.getRawAxis(2) > 0.5);
        if (otherStick.getRawButton(2)) {
            fireLow(otherStick.getRawButton(1));
        } else {
            fire(otherStick.getRawButton(1));
        }

        // Far top thumb button
        if (otherStick.getRawButton(3)) {
            ballLift.set(VictorSPXControlMode.PercentOutput, -1.0);
            ballIntake.set(VictorSPXControlMode.PercentOutput, 0.5);
        } else if (otherStick.getRawButton(4)) {
            ballLift.set(VictorSPXControlMode.PercentOutput, 1.0);
            ballIntake.set(VictorSPXControlMode.PercentOutput, -0.5);
        } else {
            ballLift.set(VictorSPXControlMode.PercentOutput, 0.0);
            ballIntake.set(VictorSPXControlMode.PercentOutput, 0);
        }
    }

    //only sets axis to joystick value if outside specified deadzone
    private double applyDeadzone(double axis) {
        if (Math.abs(axis) > 0.2) {
            if (axis < 0) {
                return (axis + 0.2);
            } else {
                return (axis - 0.2);
            }
        }
        return 0;
    }

    private void fireLow(boolean enable) {
        fireAt(enable, -2800);
    }

    private void fire(boolean enable) {
        fireAt(enable, -4700);
    }

    private void fireAt(boolean enable, double speed) {
        var velocity = shoot.getEncoder().getVelocity();
        System.out.println(velocity);

        lightControl.set(true);
        if (enable) {
            // shoot.set(0.1 * otherStick.getY());
            var mult = -otherStick.getRawAxis(3) * 0.5 + 0.5;
            shoot.getPIDController().setReference(speed - mult * 500, CANSparkMax.ControlType.kVelocity);
            // shoot.set(-1);
            lightControl.set(true);
            if (Math.abs(velocity) < 4900) {
                // System.out.println("spin spin spin");
            } else {
                // System.out.println("shoot");
            }
        } else {
            // shoot.set(0);
            if (Math.abs(velocity) < 100) {
                shoot.stopMotor();
            } else {
                shoot.getPIDController().setReference(0, CANSparkMax.ControlType.kVelocity);
            }
            // shoot.set(0);
            lightControl.set(false);
            if (Math.abs(velocity) > 10) {
                // System.out.println("woah woah slow down");
            }
        }

        // lightControl.set(false);
    }

    private void fireBig(boolean enable) {
        if (enable) {
            shoot.getPIDController().setReference(-4000, CANSparkMax.ControlType.kVelocity);
        } else {
            shoot.getPIDController().setReference(0, CANSparkMax.ControlType.kVelocity);
        }
    }

    private void drive(double y, double x, double z) {
        double multiplier;
        if (isTeleop()) {
            // multiplier = (-otherStick.getThrottle() * 0.5) + 0.5;
            multiplier = xbox.getRawAxis(3);
            var MIN = 0.3;
            multiplier *= 1 - MIN;
            multiplier += MIN;

            var brake = 1 - xbox.getRawAxis(2);
            brake *= 1 - 0.3;
            brake += 0.3;
            multiplier *= brake;
        } else {
            multiplier = 0.5;
        }
        x *= multiplier;
        y *= multiplier;
        z *= multiplier * 0.7;
        drive.driveCartesian(-y, x, -z);
    }

    private void driveAuto(float offset, float distance) {
        var off = (double) offset;
        var dist = (double) distance;
        var OFF_RANGE = 8;
        var OFF_DIV = 40;
        if (off < -OFF_RANGE) {
            off += OFF_RANGE;
            off /= OFF_DIV;
        } else if (off > OFF_RANGE) {
            off -= OFF_RANGE;
            off /= OFF_DIV;
        } else {
            off = 0;
        }
        var DIST_RANGE = 10;
        var DIST_DIV = 10;
        dist *= -1;
        if (dist < -DIST_RANGE) {
            dist += DIST_RANGE;
            dist /= DIST_DIV;
        } else if (dist > DIST_RANGE) {
            dist -= DIST_RANGE;
            dist /= DIST_DIV;
        } else {
            dist = 0;
        }
        var z = off;
        var y = dist;
        if (y > 1) {
            y = 1;
        } else if (y < -1) {
            y = -1;
        }
        y *= -1;
        if (z > 1) {
            z = 1;
        } else if (z < -1) {
            z = -1;
        }
        z *= -1;
        drive(y, 0, z);
    }
}
