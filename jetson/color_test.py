
# Python program for Detection of a
# specific color(blue here) using OpenCV with Python
import cv2
import numpy as np

# Webcamera no 0 is used to capture the frames
cap = cv2.VideoCapture(1)

# This drives the program into an infinit
upper_bound = np.array([75, 255, 75])


while True:

    # Captures the live stream frame-by-frame
    #ret, frame = cap.read()
    frame = cv2.imread('reflective_tape.jpg')
    scale_percent = 20
    dim = (int(frame.shape[1] * scale_percent / 100), int(frame.shape[0] * scale_percent / 100))
    frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    # convert to grayscale (currently unused)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # split image into three color channels (OpenCV uses BGR)
    b, g, r = cv2.split(frame)
    # blur the green channel
    blur = cv2.blur(gray, (5, 5))
    # apply binary thresholding
    ret2, thresh = cv2.threshold(blur, 140, 200, cv2.THRESH_BINARY)
    # detect the contours on the binary image using cv2.CHAIN_APPROX_NONE
    contours, hierarchy = cv2.findContours(image=thresh, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    # get min area rect
    rect = cv2.minAreaRect(contours[0])
    box = cv2.boxPoints(rect) # cv2.boxPoints(rect) for OpenCV 3.x
    box = np.int0(box)
    # draw contours on the original image
    contour_frame = frame.copy()
    cv2.drawContours(image=contour_frame, contours=[box], contourIdx=-1, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_AA)


    cv2.imshow('res', thresh)
    cv2.imshow('cnt', contour_frame)

    key = cv2.waitKey(1)
    if key == ord("q"):
        break

# Destroys all of the HighGUI windows.
cv2.destroyAllWindows()

# release the captured frame
cap.release()


"""
    _, frame = cap.read()
    _, g, _ = cv2.split(frame)
    blur = cv2.blur(g, (5, 5))
    ret, thresh = cv2.threshold(blur, 240, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # gray = np.float32(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
    cv2.imshow('thresh', thresh)
    dst = cv2.cornerHarris(thresh, 2, 3, 0, .04)

    cv2.drawContours(frame, contours, -1, (255, 0, 0), 2)
    cv2.dilate(dst, None)

    #define kernel size
    kernel = np.ones((7,7),np.uint8)
    # Remove unnecessary noise from mask
    # mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

    # The bitwise and of the frame and mask is done so
    # that only the blue coloured objects are highlighted
    # and stored in res
    # res = cv2.bitwise_and(frame,frame, mask= mask)
    # cv2.imshow('mask',mask)
    #frame[dst > 0.01*dst.max()] = [0, 0, 255]
"""