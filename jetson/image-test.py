import main
import cv2

img = cv2.imread("images/opencv-test-1.jpg")
main.process_image(img)

while True:
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q') or key == 27:
        break