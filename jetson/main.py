import cv2
import numpy as np
import os

JETSON = False

# Capture on the first available camera
def init_cam(cam_index):
    while cam_index < 5:
        c = cv2.VideoCapture(cam_index)
        res, _ = c.read()
        if res:
            return (c, cam_index)
    print("Can't find working camera")
    exit()

def draw_contours_fancy(frame, contours):
    c = max(contours, key = cv2.contourArea)

    # Simplify our contour a bit (removes errors)
    peri = cv2.arcLength(c, True)
    c = cv2.approxPolyDP(c, 0.001 * peri, True)

    # Find the bounding rectangle of the countour
    x, y, w, h = cv2.boundingRect(c)
    cv2.rectangle(frame, (x,y), (x+w,y+h), (0,255,0), 2)
    cv2.drawContours(frame, c, -1, 255, 3)

    # Find Convex Hull
    hull = cv2.convexHull(c)
    top_left     = [10000, 10000]
    top_right    = [0, 10000]
    bottom_left  = [10000, 0]
    bottom_right = [0, 0]
    for p in hull:
        point = p[0]
        if -point[0] - point[1] > -top_left[0] - top_left[1]:
            top_left = point
        if point[0] - point[1] > top_right[0] - top_right[1]:
            top_right = point
        if -point[0] + point[1] > -bottom_left[0] + bottom_left[1]:
            bottom_left = point
        if point[0] + point[1] > bottom_right[0] + bottom_right[1]:
            bottom_right = point

    cv2.line(frame, tuple(top_left),     tuple(top_right),    (0, 0, 255), 3)
    cv2.line(frame, tuple(top_right),    tuple(bottom_right), (0, 0, 255), 3)
    cv2.line(frame, tuple(bottom_right), tuple(bottom_left),  (0, 0, 255), 3)
    cv2.line(frame, tuple(bottom_left),  tuple(top_left),     (0, 0, 255), 3)

    src = np.float32([top_left, bottom_left, top_right, bottom_right])
    dst = np.float32([[0,0], [1,0], [0,1], [1,1]])
    transform = cv2.getPerspectiveTransform(src, dst)
    print(transform)
    # transform = np.linalg.inv(transform)

    points = np.array([[0, 0, 1], [2, 0, 1], [0, 5, 1], [2, 5, 1]])
    for p in points:
        world_space = np.matmul(transform, p)
        print(p)
        print(world_space)

    img = cv2.warpPerspective(frame, transform, (300,300))
    if not JETSON:
        cv2.imshow("things", img)

def draw_contours(frame, contours):
    c = max(contours, key = cv2.contourArea)

    # Simplify our contour a bit (removes errors)
    peri = cv2.arcLength(c, True)
    c = cv2.approxPolyDP(c, 0.001 * peri, True)

    # Find the bounding rectangle of the countour
    x, y, w, h = cv2.boundingRect(c)
    cv2.rectangle(frame, (x,y), (x+w,y+h), (0,255,0), 2)
    cv2.drawContours(frame, c, -1, 255, 3)

def process_image(frame):
    # Reflection Detection
    r, g, _ = cv2.split(frame)
    blur_r = cv2.blur(r, (5, 5))
    blur_g = cv2.blur(g, (5, 5))
    _, red_thresh = cv2.threshold(blur_r, 128, 255, cv2.THRESH_BINARY)
    mask = cv2.bitwise_not(red_thresh)
    masked = cv2.bitwise_and(blur_g, blur_g, mask=mask)
    frame = cv2.bitwise_and(frame, frame, mask=mask)
    brightest_pixel = np.array(masked).max()
    thresh_min = brightest_pixel * 0.5
    print(thresh_min)
    cv2.imshow("frame", frame)
    _, thresh = cv2.threshold(masked, thresh_min, 255, cv2.THRESH_BINARY)

    # Find Biggest Contour
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if contours:
        draw_contours(frame, contours)

    if not JETSON:
        cv2.imshow("frame", frame)

    return (3.23, 5.1)

def write_values(offset, distance, file):
    file.write(str(offset))
    file.write(',')
    file.write(str(distance))
    file.write(',\n') # Trailing comma is required!
    file.flush()

def set_brightness(cam_index):
    # Auto exposure to manual first
    os.system(f"v4l2-ctl --device {cam_index} -c auto_exposure=1")
    os.system(f"v4l2-ctl --device {cam_index} -c brightness=0")
    os.system(f"v4l2-ctl --device {cam_index} -c backlight_compensation=0")
    os.system(f"v4l2-ctl --device {cam_index} -c exposure_time_absolute=10")
    pass

if __name__ == "__main__":
    cap, cam_index = init_cam(2)
    set_brightness(cam_index)
    if JETSON:
        tty = open("/dev/ttyGS0", "w")

    while True:
        # Read a frame, break if it's invalid
        ret, frame = cap.read()
        if not ret:
            break

        (offset, distance) = process_image(frame)
        if JETSON:
            write_values(offset, distance, tty)

        # Close if `q` or `esc` is pressed
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q') or key == 27:
            break

    if JETSON:
        tty.close()
    cap.release()
