import cv2

# Capture on the first input (the first attached camera)
cap = cv2.VideoCapture(0)

while True:
    # Read a frame, break if it's invalid
    ret, frame = cap.read()
    if not ret:
        break

    r, g, b = cv2.split(frame)

    # lower_bound = 127
    # upper_bound = 255
    # _, thresh = cv2.threshold(r, lower_bound, upper_bound, cv2.THRESH_BINARY)

    cv2.imshow("red", r)
    cv2.imshow("green", g)
    cv2.imshow("blue", b)

    # Close if `q` or `esc` is pressed
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q') or key == 27:
        break

cap.release()
