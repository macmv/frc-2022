import cv2

# Capture on the first input (the first attached camera)
cap = cv2.VideoCapture(0)

# Empty callback to use when the trackbar is moved
def nothing(x):
    pass

WIN = "hello"

cv2.namedWindow(WIN)
cv2.createTrackbar("lower bound", WIN, 0, 255, nothing)

while True:
    # Read a frame, break if it's invalid
    ret, frame = cap.read()
    if not ret:
        break

    lower_bound = cv2.getTrackbarPos("lower bound", WIN)

    _, thresh = cv2.threshold(frame, lower_bound, 255, cv2.THRESH_BINARY)
    cv2.imshow(WIN, thresh)

    # Close if `q` or `esc` is pressed
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q') or key == 27:
        break

cap.release()
